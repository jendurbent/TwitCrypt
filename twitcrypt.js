function performEncryption(){
	// Set the encryption pad & label
		var txtEncryptPad = $("#EncryptPad").val();
		var lenEnPadLength = txtEncryptPad.length;
		$("#txtEncryptPadcount").text("Pad Length: " + (lenEnPadLength) );
	// Set the encrypted text and label
		var txtToEncrypt = $("#EncryptInput").val();
		var lenEncryptText = txtToEncrypt.length;	
		$("#txtToEncryptcount").text("Text to Encrypt (Chars Left: " + (lenEnPadLength-lenEncryptText) + ")");
	//empty container & do the encryption
		var txtEncrypted = "";	
		for (var i = 0, len = lenEncryptText; i < len; i++) {
			var txtToEncryptLetter = txtToEncrypt.charCodeAt([i]);
			var txtPadLetter = txtEncryptPad.charCodeAt([i]);
			var txtEncryptedLetter = txtToEncryptLetter + txtPadLetter;
			if (txtEncryptedLetter > 126) { //if we're out of ascii space
				txtEncryptedLetter = txtEncryptedLetter-93; //subtract until we're back at the start of it (after spaces)
			}
			if (txtEncryptedLetter > 126) { //sometimes this happens twice
				txtEncryptedLetter = txtEncryptedLetter-93; //subtract until we're back at the start of it (after spaces)
			}
			txtEncryptedLetterOut = String.fromCharCode(txtEncryptedLetter);
			txtEncrypted = txtEncrypted + txtEncryptedLetterOut	
		}
		$("#EncryptOutput").val(txtEncrypted);
}
function performDecryption(){
		var txtDecryptPad = $("#DecryptPad").val();
		var PadLength = txtDecryptPad.length;
		$("#txtDecryptPadcount").text("Pad Length: " + (PadLength) )
	// Set the Decrypted text and label
		var txtToDecrypt = $("#DecryptInput").val();
		var lenDecryptText = txtToDecrypt.length;	
		$("#txtToDecryptcount").text("Text to Decrypt (Chars Left: " + (PadLength - $("#DecryptInput").val().length) + ")");
	//empty container & do the decryption
		var txtDecrypted = "";	
		for (var i = 0, len = lenDecryptText; i < len; i++) {
			var txtToDecryptLetter = txtToDecrypt.charCodeAt([i]);
			var txtPadLetter = txtDecryptPad.charCodeAt([i]);
			var txtDecryptedLetter = txtToDecryptLetter - txtPadLetter;
			if (txtDecryptedLetter < 32) { //if we're out of ascii space
				txtDecryptedLetter = txtDecryptedLetter+93; //subtract until we're back at the start of it (after spaces)
			}
			if (txtDecryptedLetter < 32) { //sometimes this happens twice
				txtDecryptedLetter = txtDecryptedLetter+93; //subtract until we're back at the start of it (after spaces)
			}
			txtDecryptedLetterOut = String.fromCharCode(txtDecryptedLetter);
			txtDecrypted = txtDecrypted + txtDecryptedLetterOut	
		}
		$("#DecryptOutput").val(txtDecrypted);
}
$(document).ready(function(){
	//these are called to make sure the label counts are accurate based on the default pad. 
		performEncryption();
		performDecryption();
	//Encrypt the stuff on every change of the pad or the input
		$("#EncryptInput").keyup(function(){
			performEncryption();
		});
		$("#EncryptPad").keyup(function(){
			performEncryption();
		});
	//decrypt the stuff on every change of the pad or the input
		$("#DecryptPad").keyup(function(){
			performDecryption();
		});
		$("#DecryptInput").keyup(function(){
			performDecryption();
		});
})