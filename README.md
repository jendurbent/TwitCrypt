# TwitCrypt
A One Time Pad encryption thing

Pretty much what it says on the tin. I made it for Twitter and Mastodon but it's not limited to those because it works in plain text.

I am not a professional developer. This was merely created as an exercise. 

If you're using this to do anything important, that's not a good idea. 
